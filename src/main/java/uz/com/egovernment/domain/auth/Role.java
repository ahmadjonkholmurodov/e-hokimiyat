package uz.com.egovernment.domain.auth;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Where;
import org.springframework.security.core.GrantedAuthority;
import uz.com.egovernment.domain.AbstractEntity;

import javax.persistence.*;
import java.util.Collection;

@Getter
@Setter
@ToString
@Entity
@NoArgsConstructor
@Table(name = "roles")
@Where(clause = "deleted=false")
public class Role extends AbstractEntity implements GrantedAuthority {

    @Column(nullable = false)
    private String name;

    @Column(unique = true, nullable = false)
    private String code;

    private Boolean enabled;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "role_menus", joinColumns = {@JoinColumn(name = "role_id", referencedColumnName = "id", nullable = false)}
            , inverseJoinColumns = {@JoinColumn(name = "menu_id", referencedColumnName = "id", nullable = false)})
    private Collection<Menu> roleMenus;

    @Override
    public String getAuthority() {
        return "ROLE_" + code.toUpperCase();
    }

}
