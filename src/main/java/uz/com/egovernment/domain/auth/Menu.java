package uz.com.egovernment.domain.auth;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Where;
import uz.com.egovernment.domain.AbstractEntity;

import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "menus")
@Where(clause = "deleted=false")
public class Menu extends AbstractEntity {

    @Column(nullable = false)
    private String name;

    @Column
    private String nameCy;

    private Boolean isPage;

    private Boolean enabled;

    @ManyToOne(fetch = FetchType.LAZY)
    private Menu parent;
}
