package uz.com.egovernment.domain.auth;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Where;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import uz.com.egovernment.domain.AbstractEntity;
import uz.com.egovernment.dto.enums.Languages;
import uz.com.egovernment.dto.enums.Roles;
import uz.com.egovernment.dto.enums.UserStateType;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "users")
@Where(clause = "deleted=false")
public class LocalUser extends AbstractEntity implements UserDetails {

    @Column(nullable = false, unique = true)
    protected String username;

    @Column(nullable = false)
    protected String password;

    @Column(name = "email")
    protected String email;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "first_name")
    protected String firstName;

    @Column(name = "middle_name")
    protected String middleName;

    @Column(name = "last_name")
    protected String lastName;

    @Column(name = "type_state")
    @Enumerated(EnumType.ORDINAL)
    protected UserStateType state = UserStateType.ACTIVE;

    @Column(name = "language")
    @Enumerated(EnumType.ORDINAL)
    protected Languages language = Languages.UZ;


    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_roles", joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)}
            , inverseJoinColumns = {@JoinColumn(name = "role_id", referencedColumnName = "id", nullable = false)})
    private Collection<Role> roles;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authorities
                = new ArrayList<>();
        for (Role role : roles) {
            authorities.add(new SimpleGrantedAuthority(role.getCode()));
        }

        return authorities;
    }

    private boolean hasRole(Roles role) {
        return getAuthorities().contains(new SimpleGrantedAuthority(role.name()));
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return state.equals(UserStateType.ACTIVE);
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return state.equals(UserStateType.ACTIVE);
    }
}