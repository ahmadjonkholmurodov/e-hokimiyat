package uz.com.egovernment.domain.auth;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Where;
import uz.com.egovernment.domain.AbstractEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "actions")
@Where(clause = "deleted=false")
public class Action extends AbstractEntity {

    @Column(nullable = false)
    private String code;

    @Column(columnDefinition = "varchar(512) default null")
    private String description;

    @Column
    private Boolean enabled;
}
