package uz.com.egovernment.domain.place;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Where;
import uz.com.egovernment.domain.AbstractEntity;

import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "streets")
@Where(clause = "deleted=false")
public class Street extends AbstractEntity {

    @Column(nullable = false)
    private String name;

    @Column
    private String nameCy;

    @ManyToOne(fetch = FetchType.LAZY)
    private Mahalla mahalla;
}
