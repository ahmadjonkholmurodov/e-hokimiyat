package uz.com.egovernment.domain.place;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Where;
import uz.com.egovernment.domain.AbstractEntity;

import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "districts")
@Where(clause = "deleted=false")
public class District extends AbstractEntity {

    @Column(nullable = false)
    private String name;

    @Column
    private String nameCy;

    @ManyToOne(fetch = FetchType.LAZY)
    private Region region;
}
