package uz.com.egovernment.domain.place;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Where;
import uz.com.egovernment.domain.AbstractEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "regions")
@Where(clause = "deleted=false")
public class Region extends AbstractEntity {

    @Column(nullable = false)
    private String name;

    @Column
    private String nameCy;
}
