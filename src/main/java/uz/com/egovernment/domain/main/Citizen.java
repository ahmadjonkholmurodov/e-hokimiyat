package uz.com.egovernment.domain.main;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Where;
import uz.com.egovernment.domain.AbstractEntity;
import uz.com.egovernment.domain.place.*;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "citizens")
@Where(clause = "deleted=false")
public class Citizen extends AbstractEntity {

    @Column(name = "first_name")
    protected String firstName;

    @Column(name = "last_name")
    protected String lastName;

    @Column(name = "middle_name")
    protected String middleName;

    @Column(name = "first_name_cy")
    protected String firstNameCy;

    @Column(name = "last_name_cy")
    protected String lastNameCy;

    @Column(name = "middle_name_cy")
    protected String middleNameCy;

    @Column(name = "passport_serial")
    protected String passportSerial;

    @Column(name = "passport_number")
    protected String passportNumber;

    @Column
    private Date birthDate;

    @Column(name = "pinfl")
    protected String pinfl;

    @Column(name = "marital_status")
    protected String maritalStatus;

    @Column(name = "contact")
    protected String contact;

    @Column
    private Boolean isAlive;

    @ManyToOne(fetch = FetchType.LAZY)
    protected District birthDistrict;

    /**
     * residence data
     */
    @ManyToOne(fetch = FetchType.LAZY)
    protected Home residenceHome;

    @ManyToOne(fetch = FetchType.LAZY)
    protected Street residenceStreet;

    @ManyToOne(fetch = FetchType.LAZY)
    protected Mahalla residenceMahalla;

    @ManyToOne(fetch = FetchType.LAZY)
    protected Sector residenceSector;

    @ManyToOne(fetch = FetchType.LAZY)
    protected District residenceDistrict;

    @ManyToOne(fetch = FetchType.LAZY)
    protected Region residenceRegion;
}
