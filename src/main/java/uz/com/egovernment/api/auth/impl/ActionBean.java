package uz.com.egovernment.api.auth.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import uz.com.egovernment.api.auth.Actions;
import uz.com.egovernment.dao.auth.ActionRepository;
import uz.com.egovernment.domain.auth.Action;
import uz.com.egovernment.dto.auth.ActionModifyDto;
import uz.com.egovernment.exceptions.ErrorCode;
import uz.com.egovernment.exceptions.LocalizedApplicationException;
import uz.com.egovernment.pagination.auth.ActionPageableSearch;
import uz.com.egovernment.utils.DaoUtils;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

@Component
public class ActionBean implements Actions {

    private final ActionRepository actionRepository;

    @Autowired
    public ActionBean(ActionRepository actionRepository) {
        this.actionRepository = actionRepository;
    }

    @Override
    public Page<Action> list(ActionPageableSearch search) {
        return actionRepository.findAll((Specification<Action>) (root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();

            if (!ObjectUtils.isEmpty(search.getCode())) {
                predicates.add(cb.like(root.get("code"), search.getCode()));
            }

            return cb.and(predicates.toArray(new Predicate[0]));
        }, DaoUtils.toPaging(search));
    }

    @Override
    public Action get(Long actionId) {
        return actionRepository.findById(actionId).orElseThrow(() -> new LocalizedApplicationException(ErrorCode.ENTITY_NOT_FOUND, actionId));
    }

    @Override
    public Action modify(ActionModifyDto dto) {
        Action action = new Action();

        if (!ObjectUtils.isEmpty(dto.getId())) {
            action = get(dto.getId());
        }

        action.setDescription(dto.getDescription());
        action.setCode(dto.getCode());

        return actionRepository.save(action);
    }

    @Override
    public void delete(Long actionId) {
        Action action = get(actionId);
        actionRepository.delete(action);
    }
}
