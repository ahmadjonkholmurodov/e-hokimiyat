package uz.com.egovernment.api.auth;

import org.springframework.data.domain.Page;
import uz.com.egovernment.domain.auth.Action;
import uz.com.egovernment.dto.auth.ActionModifyDto;
import uz.com.egovernment.pagination.auth.ActionPageableSearch;

public interface Actions {

    Page<Action> list(ActionPageableSearch search);

    Action get(Long actionId);

    Action modify(ActionModifyDto dto);

    void delete(Long actionId);
}
