package uz.com.egovernment.api.auth.impl;

import org.springframework.stereotype.Component;
import uz.com.egovernment.api.auth.MenuActions;
import uz.com.egovernment.dao.auth.ActionRepository;
import uz.com.egovernment.dao.auth.MenuActionRepository;
import uz.com.egovernment.dao.auth.MenuRepository;
import uz.com.egovernment.dao.auth.RoleRepository;
import uz.com.egovernment.domain.auth.Action;
import uz.com.egovernment.domain.auth.Menu;
import uz.com.egovernment.domain.auth.MenuAction;
import uz.com.egovernment.domain.auth.Role;
import uz.com.egovernment.dto.auth.MenuActionCreateDto;

@Component
public class MenuActionBean implements MenuActions {

    private final MenuActionRepository menuActionRepository;
    private final RoleRepository roleRepository;
    private final ActionRepository actionRepository;
    private final MenuRepository menuRepository;

    public MenuActionBean(MenuActionRepository menuActionRepository, RoleRepository roleRepository, ActionRepository actionRepository, MenuRepository menuRepository) {
        this.menuActionRepository = menuActionRepository;
        this.roleRepository = roleRepository;
        this.actionRepository = actionRepository;
        this.menuRepository = menuRepository;
    }

    @Override
    public MenuAction create(MenuActionCreateDto dto) {
        Role role = roleRepository.getById(dto.getRoleId());
        Menu menu = menuRepository.getById(dto.getMenuId());
        Action action = actionRepository.getById(dto.getActionId());

        MenuAction menuAction = new MenuAction();
        menuAction.setAction(action);
        menuAction.setRole(role);
        menuAction.setMenu(menu);
        return menuActionRepository.save(menuAction);
    }
}
