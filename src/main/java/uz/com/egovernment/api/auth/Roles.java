package uz.com.egovernment.api.auth;

import org.springframework.data.domain.Page;
import uz.com.egovernment.domain.auth.Role;
import uz.com.egovernment.dto.auth.RoleModifyDto;
import uz.com.egovernment.pagination.auth.RolePageableSearch;

public interface Roles {

    Page<Role> list(RolePageableSearch search);

    Role get(Long roleId);

    Role modify(RoleModifyDto dto);

    void delete(Long roleId);
}
