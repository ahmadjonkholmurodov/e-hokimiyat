package uz.com.egovernment.api.auth;

import org.springframework.data.domain.Page;
import uz.com.egovernment.domain.auth.LocalUser;
import uz.com.egovernment.dto.auth.UserCreateRequestDto;
import uz.com.egovernment.dto.auth.UserInfoDto;
import uz.com.egovernment.dto.auth.UserUpdateRequestDto;
import uz.com.egovernment.pagination.auth.UserSearch;

public interface Users {

    Page<LocalUser> list(UserSearch search);

    LocalUser get(Long userId);

    LocalUser create(UserCreateRequestDto dto);

    LocalUser update(UserUpdateRequestDto dto);

    void delete(Long userId);

    UserInfoDto userInfo();

}
