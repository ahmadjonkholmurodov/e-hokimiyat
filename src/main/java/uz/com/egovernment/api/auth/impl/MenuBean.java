package uz.com.egovernment.api.auth.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import uz.com.egovernment.api.auth.Menus;
import uz.com.egovernment.dao.auth.MenuRepository;
import uz.com.egovernment.domain.auth.Menu;
import uz.com.egovernment.dto.auth.MenuModifyDto;
import uz.com.egovernment.exceptions.ErrorCode;
import uz.com.egovernment.exceptions.LocalizedApplicationException;
import uz.com.egovernment.pagination.auth.MenuPageableSearch;
import uz.com.egovernment.utils.DaoUtils;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

@Component
public class MenuBean implements Menus {

    private final MenuRepository menuRepository;

    @Autowired
    public MenuBean(MenuRepository menuRepository) {
        this.menuRepository = menuRepository;
    }

    @Override
    public Page<Menu> list(MenuPageableSearch search) {
        return menuRepository.findAll((Specification<Menu>) (root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();

            if (!ObjectUtils.isEmpty(search.getName())) {
                predicates.add(cb.like(root.get("name"), DaoUtils.toLikeCriteria(search.getName())));
            }

            if (!ObjectUtils.isEmpty(search.getParentId())) {
                predicates.add(cb.equal(root.get("parent").get("id"), search.getParentId()));
            }

            return cb.and(predicates.toArray(new Predicate[0]));
        }, DaoUtils.toPaging(search));
    }

    @Override
    public Menu get(Long menuId) {
        return menuRepository.findById(menuId).orElseThrow(() -> new LocalizedApplicationException(ErrorCode.ENTITY_NOT_FOUND, menuId));
    }

    @Override
    public Menu modify(MenuModifyDto dto) {
        Menu menu = new Menu();

        if (!ObjectUtils.isEmpty(dto.getId())) {
            menu = get(dto.getId());
        }
        if (!ObjectUtils.isEmpty(dto.getParentId())) {
            menu.setParent(get(dto.getParentId()));
        }

        menu.setName(dto.getName());
        menu.setNameCy(dto.getNameCy());
        menu.setEnabled(dto.getEnabled());
        menu.setIsPage(dto.getIsPage());
        return menuRepository.save(menu);
    }

    @Override
    public void delete(Long menuId) {
        Menu menu = get(menuId);
        menuRepository.delete(menu);
    }
}
