package uz.com.egovernment.api.auth.impl;

import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import uz.com.egovernment.api.auth.Roles;
import uz.com.egovernment.dao.auth.RoleRepository;
import uz.com.egovernment.domain.auth.Role;
import uz.com.egovernment.dto.auth.RoleModifyDto;
import uz.com.egovernment.exceptions.ErrorCode;
import uz.com.egovernment.exceptions.LocalizedApplicationException;
import uz.com.egovernment.pagination.auth.RolePageableSearch;
import uz.com.egovernment.utils.DaoUtils;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

@Component
public class RoleBean implements Roles {

    private final RoleRepository roleRepository;

    public RoleBean(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public Page<Role> list(RolePageableSearch search) {
        return roleRepository.findAll((Specification<Role>) (root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();

            if (!ObjectUtils.isEmpty(search.getName())) {
                predicates.add(cb.like(root.get("name"), search.getName()));
            }

            if (!ObjectUtils.isEmpty(search.getCode())) {
                predicates.add(cb.like(root.get("code"), search.getCode()));
            }

            return cb.and(predicates.toArray(new Predicate[0]));
        }, DaoUtils.toPaging(search));
    }

    @Override
    public Role get(Long roleId) {
        return roleRepository.findById(roleId).orElseThrow(() -> new LocalizedApplicationException(ErrorCode.ENTITY_NOT_FOUND, roleId));
    }

    @Override
    public Role modify(RoleModifyDto dto) {
        Role role = new Role();

        if (!ObjectUtils.isEmpty(dto.getId())) {
            role = get(dto.getId());
        }

        role.setName(dto.getName());
        role.setCode(dto.getCode());

        return roleRepository.save(role);
    }

    @Override
    public void delete(Long roleId) {
        Role role = get(roleId);
        roleRepository.delete(role);
    }
}
