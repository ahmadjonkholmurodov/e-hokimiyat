package uz.com.egovernment.api.auth.impl;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import uz.com.egovernment.api.auth.Security;
import uz.com.egovernment.dao.auth.UserRepository;
import uz.com.egovernment.domain.auth.LocalUser;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

import static uz.com.egovernment.exceptions.ErrorCode.USERNAME_NOT_FOUND;

@Component
public class SecurityBean implements Security {

    private final UserRepository userRepository;
    private static LocalUser currentUser;

    public SecurityBean(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public LocalUser getCurrentUser() {
        if (currentUser != null) {
            return currentUser;
        }

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            throw new UsernameNotFoundException("Should be authenticated!");
        }
        return loadUserByUsername(authentication.getName());
    }

    @Override
    public String getCurrentLanguage() {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (requestAttributes != null) {
            HttpServletRequest servletRequest = ((ServletRequestAttributes) requestAttributes).getRequest();
            String lang = servletRequest.getHeader("lang");
            return ObjectUtils.isEmpty(lang) ? "uz" : lang;
        }
        //todo define default value in application.properties
        return "uz";
    }

    @Override
    public LocalUser loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<LocalUser> userByLogin = getUserByLogin(username);
        if (userByLogin.isPresent()) {
            LocalUser localUser = userByLogin.get();
            currentUser = localUser;
            return localUser;
        } else {
            throw new UsernameNotFoundException(USERNAME_NOT_FOUND.name());
        }
    }

    private Optional<LocalUser> getUserByLogin(String username) {
        return userRepository.findFirstByUsername(username);
    }
}
