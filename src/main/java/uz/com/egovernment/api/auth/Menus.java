package uz.com.egovernment.api.auth;

import org.springframework.data.domain.Page;
import uz.com.egovernment.domain.auth.Menu;
import uz.com.egovernment.dto.auth.MenuModifyDto;
import uz.com.egovernment.pagination.auth.MenuPageableSearch;

public interface Menus {

    Page<Menu> list(MenuPageableSearch search);

    Menu get(Long menuId);

    Menu modify(MenuModifyDto dto);

    void delete(Long menuId);
}
