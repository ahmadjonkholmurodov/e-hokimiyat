package uz.com.egovernment.api.auth;

import uz.com.egovernment.domain.auth.MenuAction;
import uz.com.egovernment.dto.auth.MenuActionCreateDto;

public interface MenuActions {

    MenuAction create(MenuActionCreateDto dto);
}
