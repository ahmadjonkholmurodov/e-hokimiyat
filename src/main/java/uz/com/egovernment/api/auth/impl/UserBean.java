package uz.com.egovernment.api.auth.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import uz.com.egovernment.api.auth.Security;
import uz.com.egovernment.api.auth.Users;
import uz.com.egovernment.dao.auth.UserRepository;
import uz.com.egovernment.domain.auth.LocalUser;
import uz.com.egovernment.dto.auth.UserCreateRequestDto;
import uz.com.egovernment.dto.auth.UserInfoDto;
import uz.com.egovernment.dto.auth.UserUpdateRequestDto;
import uz.com.egovernment.exceptions.ErrorCode;
import uz.com.egovernment.exceptions.LocalizedApplicationException;
import uz.com.egovernment.pagination.auth.UserSearch;
import uz.com.egovernment.utils.DaoUtils;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class UserBean implements Users {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final Security security;

    @Autowired
    public UserBean(UserRepository userRepository, PasswordEncoder passwordEncoder, Security security) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.security = security;
    }

    @Override
    public Page<LocalUser> list(UserSearch search) {
        return userRepository.findAll((Specification<LocalUser>) (root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();

            if (!ObjectUtils.isEmpty(search.getUsername())) {
                predicates.add(cb.like(root.get("username"), DaoUtils.toLikeCriteria(search.getUsername())));
            }

            if (!ObjectUtils.isEmpty(search.getName())) {
                Predicate firstName = cb.like(root.get("firstName"), DaoUtils.toLikeCriteria(search.getName()));
                Predicate lastName = cb.like(root.get("lastName"), DaoUtils.toLikeCriteria(search.getName()));
                Predicate middleName = cb.like(root.get("middleName"), DaoUtils.toLikeCriteria(search.getName()));

                Predicate orPredicates = cb.or(firstName, lastName, middleName);
                predicates.add(cb.and(orPredicates));
            }

            return cb.and(predicates.toArray(new Predicate[0]));
        }, DaoUtils.toPaging(search));
    }

    @Override
    public LocalUser get(Long userId) {
        return userRepository.findById(userId).orElseThrow(() -> new LocalizedApplicationException(ErrorCode.ENTITY_NOT_FOUND, userId));
    }

    @Override
    public LocalUser create(UserCreateRequestDto dto) {
        checkUsernameAndEmail(dto.getUserName(), dto.getEmail());

        LocalUser localUser = new LocalUser();

        localUser.setUsername(dto.getUserName());
        localUser.setPassword(passwordEncoder.encode(dto.getPassword()));
        localUser.setEmail(dto.getEmail());
        localUser.setPhoneNumber(dto.getPhoneNumber());

        localUser.setFirstName(dto.getFirstName());
        localUser.setLastName(dto.getLastName());
        localUser.setMiddleName(dto.getMiddleName());

        return userRepository.save(localUser);
    }

    @Override
    public LocalUser update(UserUpdateRequestDto dto) {
        LocalUser localUser = get(dto.getId());
        if (ObjectUtils.isEmpty(localUser)) {
            throw new LocalizedApplicationException(ErrorCode.USERNAME_NOT_FOUND);
        }

        if (!ObjectUtils.isEmpty(dto.getUserName())) {
            Optional<LocalUser> byUsernameAndIdIsNot = userRepository.findByUsernameAndIdIsNot(dto.getUserName(), dto.getId());
            if (byUsernameAndIdIsNot.isPresent()) {
                throw new LocalizedApplicationException(ErrorCode.FIELD_VALUE_UNIQUE, "userName");
            }
            localUser.setUsername(dto.getUserName());
        }

        if (!ObjectUtils.isEmpty(dto.getEmail())) {
            Optional<LocalUser> byEmailAndIdIsNot = userRepository.findByEmailAndIdIsNot(dto.getUserName(), dto.getId());
            if (byEmailAndIdIsNot.isPresent()) {
                throw new LocalizedApplicationException(ErrorCode.FIELD_VALUE_UNIQUE, "email");
            }
            localUser.setEmail(dto.getEmail());
        }

        if (!ObjectUtils.isEmpty(dto.getPassword())) {
            localUser.setPassword(passwordEncoder.encode(dto.getUserName()));
        }

        localUser.setPhoneNumber(dto.getPhoneNumber());
        localUser.setLanguage(dto.getLanguage());
        localUser.setFirstName(dto.getFirstName());
        localUser.setLastName(dto.getLastName());
        localUser.setMiddleName(dto.getMiddleName());

        return userRepository.save(localUser);
    }

    @Override
    public void delete(Long userId) {
        userRepository.deleteById(userId);
    }

    @Override
    public UserInfoDto userInfo() {
        LocalUser currentUser = security.getCurrentUser();
        UserInfoDto infoDto = new UserInfoDto();
        infoDto.setId(currentUser.getId());

        infoDto.setFirstName(currentUser.getFirstName());
        infoDto.setLastName(currentUser.getLastName());
        infoDto.setMiddleName(currentUser.getMiddleName());
        infoDto.setLanguage(currentUser.getLanguage());
        infoDto.setState(currentUser.getState());

        currentUser.getRoles().forEach(userRole -> {
            infoDto.getRoles().add(userRole.getCode());
        });
        return infoDto;
    }

    private void checkUsernameAndEmail(String userName, String email) {
        if (userRepository.findByUsernameNative(userName).isPresent()) {
            throw new LocalizedApplicationException(ErrorCode.FIELD_VALUE_UNIQUE, "userName");
        }
        if (userRepository.findByEmailNative(email).isPresent()) {
            throw new LocalizedApplicationException(ErrorCode.FIELD_VALUE_UNIQUE, "email");
        }
    }
}
