package uz.com.egovernment.api.auth;

import org.springframework.security.core.userdetails.UserDetailsService;
import uz.com.egovernment.domain.auth.LocalUser;

public interface Security extends UserDetailsService {

    LocalUser getCurrentUser();

    String getCurrentLanguage();
}
