package uz.com.egovernment.api.place.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import uz.com.egovernment.api.place.Regions;
import uz.com.egovernment.dao.place.RegionRepository;
import uz.com.egovernment.domain.place.Region;
import uz.com.egovernment.dto.place.RegionModifyDto;
import uz.com.egovernment.exceptions.ErrorCode;
import uz.com.egovernment.exceptions.LocalizedApplicationException;
import uz.com.egovernment.pagination.place.RegionPageableSearch;
import uz.com.egovernment.utils.DaoUtils;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

@Component
public class RegionBean implements Regions {

    private final RegionRepository regionRepository;

    @Autowired
    public RegionBean(RegionRepository regionRepository) {
        this.regionRepository = regionRepository;
    }

    @Override
    public Page<Region> list(RegionPageableSearch search) {
        return regionRepository.findAll((Specification<Region>) (root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();

            if (!ObjectUtils.isEmpty(search.getName())) {
                predicates.add(cb.like(root.get("name"), search.getName()));
            }

            return cb.and(predicates.toArray(new Predicate[0]));
        }, DaoUtils.toPaging(search));
    }

    @Override
    public Region get(Long regionId) {
        return regionRepository.findById(regionId).orElseThrow(() -> new LocalizedApplicationException(ErrorCode.ENTITY_NOT_FOUND, regionId));
    }

    @Override
    public Region modify(RegionModifyDto dto) {
        Region region = new Region();

        if (!ObjectUtils.isEmpty(dto.getId())) {
            region = get(dto.getId());
        }

        region.setName(dto.getName());
        region.setNameCy(dto.getNameCy());

        return regionRepository.save(region);
    }

    @Override
    public void delete(Long regionId) {
        Region region = get(regionId);
        regionRepository.delete(region);
    }
}
