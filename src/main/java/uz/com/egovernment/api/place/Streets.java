package uz.com.egovernment.api.place;

import org.springframework.data.domain.Page;
import uz.com.egovernment.domain.place.Street;
import uz.com.egovernment.dto.place.StreetModifyDto;
import uz.com.egovernment.pagination.place.StreetPageableSearch;

public interface Streets {

    Page<Street> list(StreetPageableSearch search);

    Street get(Long streetId);

    Street modify(StreetModifyDto dto);

    void delete(Long streetId);
}
