package uz.com.egovernment.api.place;

import org.springframework.data.domain.Page;
import uz.com.egovernment.domain.place.Sector;
import uz.com.egovernment.dto.place.SectorModifyDto;
import uz.com.egovernment.pagination.place.SectorPageableSearch;

public interface Sectors {
    Page<Sector> list(SectorPageableSearch search);

    Sector get(Long sectorId);

    Sector modify(SectorModifyDto dto);

    void delete(Long sectorId);
}
