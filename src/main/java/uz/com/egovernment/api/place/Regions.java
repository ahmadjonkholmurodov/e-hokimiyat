package uz.com.egovernment.api.place;

import org.springframework.data.domain.Page;
import uz.com.egovernment.domain.place.Region;
import uz.com.egovernment.dto.place.RegionModifyDto;
import uz.com.egovernment.pagination.place.RegionPageableSearch;

public interface Regions {

    Page<Region> list(RegionPageableSearch search);

    Region get(Long regionId);

    Region modify(RegionModifyDto dto);

    void delete(Long regionId);
}
