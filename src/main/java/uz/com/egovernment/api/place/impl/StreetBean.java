package uz.com.egovernment.api.place.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import uz.com.egovernment.api.place.Mahallas;
import uz.com.egovernment.api.place.Streets;
import uz.com.egovernment.dao.place.StreetRepository;
import uz.com.egovernment.domain.place.Mahalla;
import uz.com.egovernment.domain.place.Street;
import uz.com.egovernment.dto.place.StreetModifyDto;
import uz.com.egovernment.exceptions.ErrorCode;
import uz.com.egovernment.exceptions.LocalizedApplicationException;
import uz.com.egovernment.pagination.place.StreetPageableSearch;
import uz.com.egovernment.utils.DaoUtils;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

@Component
public class StreetBean implements Streets {

    private final StreetRepository streetRepository;
    private final Mahallas mahallas;

    @Autowired
    public StreetBean(StreetRepository streetRepository, Mahallas mahallas) {
        this.streetRepository = streetRepository;
        this.mahallas = mahallas;
    }

    @Override
    public Page<Street> list(StreetPageableSearch search) {
        return streetRepository.findAll((Specification<Street>) (root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();

            if (!ObjectUtils.isEmpty(search.getName())) {
                predicates.add(cb.like(root.get("name"), search.getName()));
            }

            if (!ObjectUtils.isEmpty(search.getMahallaId())) {
                predicates.add(cb.equal(root.get("mahalla").get("id"), search.getMahallaId()));
            }

            return cb.and(predicates.toArray(new Predicate[0]));
        }, DaoUtils.toPaging(search));
    }

    @Override
    public Street get(Long streetId) {
        return streetRepository.findById(streetId).orElseThrow(() -> new LocalizedApplicationException(ErrorCode.ENTITY_NOT_FOUND, streetId));
    }

    @Override
    public Street modify(StreetModifyDto dto) {
        Street street = new Street();

        if (!ObjectUtils.isEmpty(dto.getId())) {
            street = get(dto.getId());
        }
        if (!ObjectUtils.isEmpty(dto.getMahallaId())) {
            Mahalla mahalla = mahallas.get(dto.getMahallaId());
            street.setMahalla(mahalla);
        }

        street.setName(dto.getName());
        street.setNameCy(dto.getNameCy());

        return streetRepository.save(street);
    }

    @Override
    public void delete(Long streetId) {
        Street street = get(streetId);
        streetRepository.delete(street);
    }
}
