package uz.com.egovernment.api.place.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import uz.com.egovernment.api.place.Districts;
import uz.com.egovernment.api.place.Regions;
import uz.com.egovernment.dao.place.DistrictRepository;
import uz.com.egovernment.domain.place.District;
import uz.com.egovernment.domain.place.Region;
import uz.com.egovernment.dto.place.DistrictModifyDto;
import uz.com.egovernment.exceptions.ErrorCode;
import uz.com.egovernment.exceptions.LocalizedApplicationException;
import uz.com.egovernment.pagination.place.DistrictPageableSearch;
import uz.com.egovernment.utils.DaoUtils;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

@Component
public class DistrictBean implements Districts {

    private final DistrictRepository districtRepository;
    private final Regions regions;

    @Autowired
    public DistrictBean(DistrictRepository districtRepository, Regions regions) {
        this.districtRepository = districtRepository;
        this.regions = regions;
    }

    @Override
    public Page<District> list(DistrictPageableSearch search) {
        return districtRepository.findAll((Specification<District>) (root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();

            if (!ObjectUtils.isEmpty(search.getName())) {
                predicates.add(cb.like(root.get("name"), search.getName()));
            }

            if (!ObjectUtils.isEmpty(search.getRegionId())) {
                predicates.add(cb.equal(root.get("region").get("id"), search.getRegionId()));
            }

            return cb.and(predicates.toArray(new Predicate[0]));
        }, DaoUtils.toPaging(search));
    }

    @Override
    public District get(Long districtId) {
        return districtRepository.findById(districtId).orElseThrow(() -> new LocalizedApplicationException(ErrorCode.ENTITY_NOT_FOUND, districtId));
    }

    @Override
    public District modify(DistrictModifyDto dto) {
        District district = new District();

        if (!ObjectUtils.isEmpty(dto.getId())) {
            district = get(dto.getId());
        }
        if (!ObjectUtils.isEmpty(dto.getRegionId())) {
            Region region = regions.get(dto.getRegionId());
            district.setRegion(region);
        }

        district.setName(dto.getName());
        district.setNameCy(dto.getNameCy());

        return districtRepository.save(district);
    }

    @Override
    public void delete(Long districtId) {
        District district = get(districtId);
        districtRepository.delete(district);
    }
}
