package uz.com.egovernment.api.place.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import uz.com.egovernment.api.place.Districts;
import uz.com.egovernment.api.place.Sectors;
import uz.com.egovernment.dao.place.SectorRepository;
import uz.com.egovernment.domain.place.District;
import uz.com.egovernment.domain.place.Sector;
import uz.com.egovernment.dto.place.SectorModifyDto;
import uz.com.egovernment.exceptions.ErrorCode;
import uz.com.egovernment.exceptions.LocalizedApplicationException;
import uz.com.egovernment.pagination.place.SectorPageableSearch;
import uz.com.egovernment.utils.DaoUtils;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

@Component
public class SectorBean implements Sectors {

    private final SectorRepository sectorRepository;
    private final Districts districts;

    @Autowired
    public SectorBean(SectorRepository sectorRepository, Districts districts) {
        this.sectorRepository = sectorRepository;
        this.districts = districts;
    }

    @Override
    public Page<Sector> list(SectorPageableSearch search) {
        return sectorRepository.findAll((Specification<Sector>) (root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();

            if (!ObjectUtils.isEmpty(search.getName())) {
                predicates.add(cb.like(root.get("name"), search.getName()));
            }

            if (!ObjectUtils.isEmpty(search.getDistrictId())) {
                predicates.add(cb.equal(root.get("district").get("id"), search.getDistrictId()));
            }

            return cb.and(predicates.toArray(new Predicate[0]));
        }, DaoUtils.toPaging(search));
    }

    @Override
    public Sector get(Long sectorId) {
        return sectorRepository.findById(sectorId).orElseThrow(() -> new LocalizedApplicationException(ErrorCode.ENTITY_NOT_FOUND, sectorId));
    }

    @Override
    public Sector modify(SectorModifyDto dto) {
        Sector sector = new Sector();

        if (!ObjectUtils.isEmpty(dto.getId())) {
            sector = get(dto.getId());
        }
        if (!ObjectUtils.isEmpty(dto.getDistrictId())) {
            District district = districts.get(dto.getDistrictId());
            sector.setDistrict(district);
        }

        sector.setName(dto.getName());
        sector.setNameCy(dto.getNameCy());

        return sectorRepository.save(sector);
    }

    @Override
    public void delete(Long sectorId) {
        Sector sector = get(sectorId);
        sectorRepository.delete(sector);
    }
}
