package uz.com.egovernment.api.place;

import org.springframework.data.domain.Page;
import uz.com.egovernment.domain.place.District;
import uz.com.egovernment.dto.place.DistrictModifyDto;
import uz.com.egovernment.pagination.place.DistrictPageableSearch;

public interface Districts {

    Page<District> list(DistrictPageableSearch search);

    District get(Long districtId);

    District modify(DistrictModifyDto dto);

    void delete(Long districtId);
}
