package uz.com.egovernment.api.place.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import uz.com.egovernment.api.place.Mahallas;
import uz.com.egovernment.api.place.Sectors;
import uz.com.egovernment.dao.place.MahallaRepository;
import uz.com.egovernment.domain.place.Mahalla;
import uz.com.egovernment.domain.place.Sector;
import uz.com.egovernment.dto.place.MahallaModifyDto;
import uz.com.egovernment.exceptions.ErrorCode;
import uz.com.egovernment.exceptions.LocalizedApplicationException;
import uz.com.egovernment.pagination.place.MahallaPageableSearch;
import uz.com.egovernment.utils.DaoUtils;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

@Component
public class MahallaBean implements Mahallas {

    private final MahallaRepository mahallaRepository;
    private final Sectors sectors;

    @Autowired
    public MahallaBean(MahallaRepository mahallaRepository, Sectors sectors) {
        this.mahallaRepository = mahallaRepository;
        this.sectors = sectors;
    }

    @Override
    public Page<Mahalla> list(MahallaPageableSearch search) {
        return mahallaRepository.findAll((Specification<Mahalla>) (root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();

            if (!ObjectUtils.isEmpty(search.getName())) {
                predicates.add(cb.like(root.get("name"), search.getName()));
            }

            if (!ObjectUtils.isEmpty(search.getSectorId())) {
                predicates.add(cb.equal(root.get("sector").get("id"), search.getSectorId()));
            }

            return cb.and(predicates.toArray(new Predicate[0]));
        }, DaoUtils.toPaging(search));
    }

    @Override
    public Mahalla get(Long mahallaId) {
        return mahallaRepository.findById(mahallaId).orElseThrow(() -> new LocalizedApplicationException(ErrorCode.ENTITY_NOT_FOUND, mahallaId));
    }

    @Override
    public Mahalla modify(MahallaModifyDto dto) {
        Mahalla mahalla = new Mahalla();

        if (!ObjectUtils.isEmpty(dto.getId())) {
            mahalla = get(dto.getId());
        }
        if (!ObjectUtils.isEmpty(dto.getSectorId())) {
            Sector sector = sectors.get(dto.getSectorId());
            mahalla.setSector(sector);
        }

        mahalla.setName(dto.getName());
        mahalla.setNameCy(dto.getNameCy());

        return mahallaRepository.save(mahalla);
    }

    @Override
    public void delete(Long mahallaId) {
        Mahalla mahalla = get(mahallaId);
        mahallaRepository.delete(mahalla);
    }
}
