package uz.com.egovernment.api.place;

import org.springframework.data.domain.Page;
import uz.com.egovernment.domain.place.Mahalla;
import uz.com.egovernment.dto.place.MahallaModifyDto;
import uz.com.egovernment.pagination.place.MahallaPageableSearch;

public interface Mahallas {

    Page<Mahalla> list(MahallaPageableSearch search);

    Mahalla get(Long mahallaId);

    Mahalla modify(MahallaModifyDto dto);

    void delete(Long mahallaId);
}
