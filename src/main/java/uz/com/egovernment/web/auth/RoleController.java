package uz.com.egovernment.web.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import uz.com.egovernment.api.auth.Roles;
import uz.com.egovernment.domain.auth.Role;
import uz.com.egovernment.dto.auth.RoleDto;
import uz.com.egovernment.dto.auth.RoleModifyDto;
import uz.com.egovernment.mapper.auth.RoleMapper;
import uz.com.egovernment.pagination.auth.RolePageableSearch;

@Slf4j
@RestController
@RequestMapping("api/roles")
@ApiModel("Точка доступа для управления ролями")
public class RoleController {

    private final Roles roles;
    private final RoleMapper roleMapper;

    public RoleController(Roles roles, RoleMapper roleMapper) {
        this.roles = roles;
        this.roleMapper = roleMapper;
    }

    @PostMapping(value = "search", produces = "application/json")
    @ApiOperation(value = "Получение списка пользователей с пагинацией и сортировкой", response = RoleDto.class, responseContainer = "List")
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Page<RoleDto> list(
            @ApiParam("Запрос списка") @RequestBody RolePageableSearch search
    ) {
        log.trace("Accessing GET api/roles/search/{}", search);

        Page<Role> foundRoles = roles.list(search);

        log.trace("{} roles found and {} returned to front-end", foundRoles.getTotalElements(), foundRoles.getSize());

        return foundRoles.map(roleMapper::toDto);
    }

    @GetMapping(value = "get/{roleId}", produces = "application/json")
    @ApiOperation(value = "Возврат роли по идентификатору", response = RoleDto.class, responseContainer = "One")
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public RoleDto get(
            @ApiParam(value = "Идентификатор", example = "1") @PathVariable Long roleId
    ) {
        log.trace("Accessing GET api/roles/get/{}", roleId);

        Role role = roles.get(roleId);

        log.trace("{} get role and returned to front-end", role);

        return roleMapper.toDto(role);
    }

    @PostMapping(value = "modify", produces = "application/json")
    @ApiOperation(value = "Создание или редактирование роли на основе предоставленной информации", response = RoleDto.class, responseContainer = "One")
    @Transactional
    public RoleDto modify(
            @ApiParam("Запрос объекта") @RequestBody RoleModifyDto dto
    ) {
        log.trace("Accessing POST api/roles/modify/{} ", dto.toJsonString());

        Role role = roles.modify(dto);

        log.trace("Role with roleId={} modify role and returned to front-end", role.getId());

        return roleMapper.toDto(role);
    }

    @DeleteMapping(value = "delete/{roleId}", produces = "application/json")
    @ApiOperation(value = "Удалить роли с заданным идентификатором", responseContainer = "void")
    @Transactional
    public void delete(
            @ApiParam(value = "Запрос списка", example = "1") @PathVariable Long roleId
    ) {
        log.trace("Accessing GET api/roles");

        roles.delete(roleId);

        log.trace("{} deleted role and returned void to front-end", roleId);
    }
}
