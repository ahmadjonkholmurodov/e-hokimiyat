package uz.com.egovernment.web.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.com.egovernment.api.auth.MenuActions;
import uz.com.egovernment.domain.auth.MenuAction;
import uz.com.egovernment.dto.auth.MenuActionCreateDto;

@Slf4j
@RestController
@RequestMapping("api/menu-actions")
@ApiModel("Точка доступа для управления действия меню")
public class MenuActionController {
    private final MenuActions menuActions;

    public MenuActionController(MenuActions menuActions) {
        this.menuActions = menuActions;
    }

    @PostMapping(value = "create", produces = "application/json")
    @ApiOperation(value = "Создание действие меню на основе предоставленной информации")
    @Transactional
    public void create(
            @ApiParam("Запрос объекта") @RequestBody MenuActionCreateDto dto
    ) {
        log.trace("Accessing POST api/menu-actions/modify/{} ", dto.toJsonString());

        MenuAction menuAction = menuActions.create(dto);

        log.trace("MenuAction with action={} create menu-action and returned to front-end", menuAction.getId());
    }
}
