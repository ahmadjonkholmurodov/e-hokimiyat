package uz.com.egovernment.web.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import uz.com.egovernment.api.auth.Users;
import uz.com.egovernment.domain.auth.LocalUser;
import uz.com.egovernment.dto.auth.UserCreateRequestDto;
import uz.com.egovernment.dto.auth.UserDto;
import uz.com.egovernment.dto.auth.UserUpdateRequestDto;
import uz.com.egovernment.mapper.auth.UserMapper;
import uz.com.egovernment.pagination.auth.UserSearch;

@Slf4j
@RestController
@RequestMapping("api/users")
@ApiModel("Точка доступа для управления Пользователя")
public class UserController {

    private final Users users;
    private final UserMapper userMapper;

    @Autowired
    public UserController(Users users, UserMapper userMapper) {
        this.users = users;
        this.userMapper = userMapper;
    }

    @PostMapping(value = "list", produces = "application/json")
    @ApiOperation(value = "Получение списка пользователей с пагинацией и сортировкой", response = UserDto.class, responseContainer = "List")
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Page<UserDto> list(
            @ApiParam("Запрос списка") @RequestBody UserSearch search
    ) {
        log.trace("Accessing GET api/users/search/{}", search);

        Page<LocalUser> foundUser = users.list(search);

        log.trace("{} users found and {} returned to front-end", foundUser.getTotalElements(), foundUser.getSize());

        return foundUser.map(userMapper::toDto);
    }

    @GetMapping(value = "get/{userId}", produces = "application/json")
    @ApiOperation(value = "Возврат пользователя по идентификатору", response = UserDto.class, responseContainer = "One")
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public UserDto get(
            @ApiParam(value = "Идентификатор", example = "1") @PathVariable Long userId
    ) {
        log.trace("Accessing GET api/users/get/{}", userId);

        LocalUser localUser = users.get(userId);

        log.trace("{} get users and returned to front-end", localUser);

        return userMapper.toDto(localUser);
    }

    @PostMapping(value = "create", produces = "application/json")
    @ApiOperation(value = "Создание пользователя на основе предоставленной информации", response = UserDto.class, responseContainer = "One")
    @Transactional
    public UserDto create(
            @ApiParam("Запрос объекта") @RequestBody UserCreateRequestDto dto
    ) {
        log.trace("Accessing POST api/users/create/{} ", dto.toJsonString());

        LocalUser localUser = users.create(dto);

        log.trace("{} created user and returned to front-end", localUser);

        return userMapper.toDto(localUser);
    }

    @PostMapping(value = "update", produces = "application/json")
    @ApiOperation(value = "Редактирование пользователя на основе предоставленной информации", response = UserDto.class, responseContainer = "One")
    @Transactional
    public UserDto update(
            @ApiParam("Запрос объекта") @RequestBody UserUpdateRequestDto dto
    ) {
        log.trace("Accessing POST api/users/update/{} ", dto.toJsonString());

        LocalUser localUser = users.update(dto);

        log.trace("{} updated user and returned to front-end", localUser);

        return userMapper.toDto(localUser);
    }

    @DeleteMapping(value = "delete/{userId}", produces = "application/json")
    @ApiOperation(value = "Удалить пользователя с заданным идентификатором", responseContainer = "void")
    @Transactional
    public void delete(
            @ApiParam(value = "Запрос списка", example = "1") @PathVariable Long userId
    ) {
        log.trace("Accessing GET api/users");

        users.delete(userId);

        log.trace("{} deleted user and returned void to front-end", userId);
    }
}
