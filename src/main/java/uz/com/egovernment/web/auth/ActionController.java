package uz.com.egovernment.web.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import uz.com.egovernment.api.auth.Actions;
import uz.com.egovernment.domain.auth.Action;
import uz.com.egovernment.dto.auth.ActionDto;
import uz.com.egovernment.dto.auth.ActionModifyDto;
import uz.com.egovernment.mapper.auth.ActionMapper;
import uz.com.egovernment.pagination.auth.ActionPageableSearch;

@Slf4j
@RestController
@RequestMapping("api/actions")
@ApiModel("Точка доступа для управления действие")
public class ActionController {

    private final Actions actions;
    private final ActionMapper actionMapper;

    @Autowired
    public ActionController(Actions actions, ActionMapper actionMapper) {
        this.actions = actions;
        this.actionMapper = actionMapper;
    }

    @PostMapping(value = "search", produces = "application/json")
    @ApiOperation(value = "Получение списка действия с пагинацией и сортировкой", response = ActionDto.class, responseContainer = "List")
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Page<ActionDto> list(
            @ApiParam("Запрос списка") @RequestBody ActionPageableSearch search
    ) {
        log.trace("Accessing GET api/actions/search/{}", search);

        Page<Action> foundActions = actions.list(search);

        log.trace("{} actions found and {} returned to front-end", foundActions.getTotalElements(), foundActions.getSize());

        return foundActions.map(actionMapper::toDto);
    }

    @GetMapping(value = "get/{actionId}", produces = "application/json")
    @ApiOperation(value = "Возврат действие по идентификатору", response = ActionDto.class, responseContainer = "One")
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public ActionDto get(
            @ApiParam(value = "Идентификатор", example = "1") @PathVariable Long actionId
    ) {
        log.trace("Accessing GET api/actions/get/{}", actionId);

        Action action = actions.get(actionId);

        log.trace("{} get action and returned to front-end", action);

        return actionMapper.toDto(action);
    }

    @PostMapping(value = "modify", produces = "application/json")
    @ApiOperation(value = "Создание или редактирование действие на основе предоставленной информации", response = ActionDto.class, responseContainer = "One")
    @Transactional
    public ActionDto modify(
            @ApiParam("Запрос объекта") @RequestBody ActionModifyDto dto
    ) {
        log.trace("Accessing POST api/actions/modify/{} ", dto.toJsonString());

        Action modify = actions.modify(dto);

        log.trace("Action with actionId={} modify action and returned to front-end", modify.getId());

        return actionMapper.toDto(modify);
    }

    @DeleteMapping(value = "delete/{actionId}", produces = "application/json")
    @ApiOperation(value = "Удалить действие с заданным идентификатором", responseContainer = "void")
    @Transactional
    public void delete(
            @ApiParam(value = "Запрос списка", example = "1") @PathVariable Long actionId
    ) {
        log.trace("Accessing GET api/actions");

        actions.delete(actionId);

        log.trace("{} deleted action and returned void to front-end", actionId);
    }
}
