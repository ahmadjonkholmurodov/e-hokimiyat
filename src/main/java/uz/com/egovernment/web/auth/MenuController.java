package uz.com.egovernment.web.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import uz.com.egovernment.api.auth.Menus;
import uz.com.egovernment.domain.auth.Menu;
import uz.com.egovernment.dto.auth.MenuDto;
import uz.com.egovernment.dto.auth.MenuModifyDto;
import uz.com.egovernment.mapper.auth.MenuMapper;
import uz.com.egovernment.pagination.auth.MenuPageableSearch;

@Slf4j
@RestController
@RequestMapping("api/menus")
@ApiModel("Точка доступа для управления меню")
public class MenuController {

    private final Menus menus;
    private final MenuMapper menuMapper;

    @Autowired
    public MenuController(Menus menus, MenuMapper menuMapper) {
        this.menus = menus;
        this.menuMapper = menuMapper;
    }

    @PostMapping(value = "search", produces = "application/json")
    @ApiOperation(value = "Получение списка действия с пагинацией и сортировкой", response = MenuDto.class, responseContainer = "List")
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Page<MenuDto> list(
            @ApiParam("Запрос списка") @RequestBody MenuPageableSearch search
    ) {
        log.trace("Accessing GET api/menus/search/{}", search);

        Page<Menu> foundMenus = menus.list(search);

        log.trace("{} menus found and {} returned to front-end", foundMenus.getTotalElements(), foundMenus.getSize());

        return foundMenus.map(menuMapper::toDto);
    }

    @GetMapping(value = "get/{menuId}", produces = "application/json")
    @ApiOperation(value = "Возврат меню по идентификатору", response = MenuDto.class, responseContainer = "One")
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public MenuDto get(
            @ApiParam(value = "Идентификатор", example = "1") @PathVariable Long menuId
    ) {
        log.trace("Accessing GET api/menus/get/{}", menuId);

        Menu menu = menus.get(menuId);

        log.trace("{} get menu and returned to front-end", menu);

        return menuMapper.toDto(menu);
    }

    @PostMapping(value = "modify", produces = "application/json")
    @ApiOperation(value = "Создание или редактирование меню на основе предоставленной информации", response = MenuDto.class, responseContainer = "One")
    @Transactional
    public MenuDto modify(
            @ApiParam("Запрос объекта") @RequestBody MenuModifyDto dto
    ) {
        log.trace("Accessing POST api/menus/modify/{} ", dto.toJsonString());

        Menu modify = menus.modify(dto);

        log.trace("Menu with menuId={} modify menu and returned to front-end", modify.getId());

        return menuMapper.toDto(modify);
    }

    @DeleteMapping(value = "delete/{menuId}", produces = "application/json")
    @ApiOperation(value = "Удалить меню с заданным идентификатором", responseContainer = "void")
    @Transactional
    public void delete(
            @ApiParam(value = "Запрос списка", example = "1") @PathVariable Long menuId
    ) {
        log.trace("Accessing GET api/menus");

        menus.delete(menuId);

        log.trace("{} deleted menu and returned void to front-end", menuId);
    }
}
