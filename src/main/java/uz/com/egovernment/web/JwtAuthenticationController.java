package uz.com.egovernment.web;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import uz.com.egovernment.api.auth.Security;
import uz.com.egovernment.api.auth.Users;
import uz.com.egovernment.configuration.auth.JwtTokenUtil;
import uz.com.egovernment.domain.auth.LocalUser;
import uz.com.egovernment.dto.auth.*;
import uz.com.egovernment.mapper.auth.UserMapper;

@Slf4j
@CrossOrigin
@RestController
@Api("Контроллер для аутентификации пользователя приложения")
public class JwtAuthenticationController {

    private final AuthenticationManager authenticationManager;
    private final JwtTokenUtil jwtTokenUtil;
    private final Security security;
    private final Users users;
    private final UserMapper userMapper;

    @Autowired
    public JwtAuthenticationController(AuthenticationManager authenticationManager, JwtTokenUtil jwtTokenUtil, Security security, Users users, UserMapper userMapper) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenUtil = jwtTokenUtil;
        this.security = security;
        this.users = users;
        this.userMapper = userMapper;
    }

    @PostMapping("/api/auth")
    @ApiOperation(value = "Аутентификация пользователя по логину и пароль, отправка в ответ токена, который будет использоваться для общения с сервером", response = LoginResponse.class)
    public LoginResponse createAuthenticationToken(@ApiParam("Запрос на аутентификацию в формате login+password") @RequestBody JwtRequest authenticationRequest) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authenticationRequest.getUsername(), authenticationRequest.getPassword()));
        } catch (Exception e) {
            throw new AccessDeniedException("Can not authenticate", e);
        }

        UserDetails userDetails = security.loadUserByUsername(authenticationRequest.getUsername());

        String token = jwtTokenUtil.generateToken(userDetails);

        UserInfoDto userInfoDto = users.userInfo();

        return new LoginResponse(token, userInfoDto);
    }

    @PostMapping("/api/registration")
    @ApiOperation(value = "Регистрация.")
    public UserDto registrationClient(@RequestBody UserCreateRequestDto request) {
        log.trace("Accessing POST /api/registration-client/{} ", request.toString());

        LocalUser localUser = users.create(request);

        log.trace("{} created user as client state=ACTIVE and returned to front-end", localUser);

        return userMapper.toDto(localUser);
    }
}
