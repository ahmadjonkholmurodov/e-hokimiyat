package uz.com.egovernment.web.place;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import uz.com.egovernment.api.place.Streets;
import uz.com.egovernment.domain.place.Street;
import uz.com.egovernment.dto.place.StreetDto;
import uz.com.egovernment.dto.place.StreetModifyDto;
import uz.com.egovernment.mapper.place.StreetMapper;
import uz.com.egovernment.pagination.place.StreetPageableSearch;

@Slf4j
@RestController
@RequestMapping("api/streets")
@ApiModel("Точка доступа для Улица")
public class StreetController {

    private final Streets streets;
    private final StreetMapper streetMapper;

    @Autowired
    public StreetController(Streets streets, StreetMapper streetMapper) {
        this.streets = streets;
        this.streetMapper = streetMapper;
    }

    @PostMapping(value = "search", produces = "application/json")
    @ApiOperation(value = "Получение списка пользователей с пагинацией и сортировкой", response = StreetDto.class, responseContainer = "List")
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Page<StreetDto> list(
            @ApiParam("Запрос списка") @RequestBody StreetPageableSearch search
    ) {
        log.trace("Accessing GET api/streets/search/{}", search);

        Page<Street> foundStreets = streets.list(search);

        log.trace("{} streets found and {} returned to front-end", foundStreets.getTotalElements(), foundStreets.getSize());

        return foundStreets.map(streetMapper::toDto);
    }

    @GetMapping(value = "get/{streetId}", produces = "application/json")
    @ApiOperation(value = "Возврат Улица по идентификатору", response = StreetDto.class, responseContainer = "One")
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public StreetDto get(
            @ApiParam(value = "Идентификатор", example = "1") @PathVariable Long streetId
    ) {
        log.trace("Accessing GET api/streets/get/{}", streetId);

        Street street = streets.get(streetId);

        log.trace("{} get street and returned to front-end", street);

        return streetMapper.toDto(street);
    }

    @PostMapping(value = "modify", produces = "application/json")
    @ApiOperation(value = "Создание или редактирование Улица на основе предоставленной информации", response = StreetDto.class, responseContainer = "One")
    @Transactional
    public StreetDto modify(
            @ApiParam("Запрос объекта") @RequestBody StreetModifyDto dto
    ) {
        log.trace("Accessing POST api/streets/modify/{} ", dto.toJsonString());

        Street street = streets.modify(dto);

        log.trace("Street with streetId={} modify street and returned to front-end", street.getId());

        return streetMapper.toDto(street);
    }

    @DeleteMapping(value = "delete/{streetId}", produces = "application/json")
    @ApiOperation(value = "Удалить Улица с заданным идентификатором", responseContainer = "void")
    @Transactional
    public void delete(
            @ApiParam(value = "Запрос списка", example = "1") @PathVariable Long streetId
    ) {
        log.trace("Accessing GET api/streets");

        streets.delete(streetId);

        log.trace("{} deleted street and returned void to front-end", streetId);
    }
}
