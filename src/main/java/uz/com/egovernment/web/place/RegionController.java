package uz.com.egovernment.web.place;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import uz.com.egovernment.api.place.Regions;
import uz.com.egovernment.domain.place.Region;
import uz.com.egovernment.dto.place.RegionDto;
import uz.com.egovernment.dto.place.RegionModifyDto;
import uz.com.egovernment.mapper.place.RegionMapper;
import uz.com.egovernment.pagination.place.RegionPageableSearch;

@Slf4j
@RestController
@RequestMapping("api/regions")
@ApiModel("Точка доступа для область")
public class RegionController {

    private final Regions regions;
    private final RegionMapper regionMapper;

    @Autowired
    public RegionController(Regions regions, RegionMapper regionMapper) {
        this.regions = regions;
        this.regionMapper = regionMapper;
    }

    @PostMapping(value = "search", produces = "application/json")
    @ApiOperation(value = "Получение списка пользователей с пагинацией и сортировкой", response = RegionDto.class, responseContainer = "List")
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Page<RegionDto> list(
            @ApiParam("Запрос списка") @RequestBody RegionPageableSearch search
    ) {
        log.trace("Accessing GET api/regions/search/{}", search);

        Page<Region> foundRegions = regions.list(search);

        log.trace("{} regions found and {} returned to front-end", foundRegions.getTotalElements(), foundRegions.getSize());

        return foundRegions.map(regionMapper::toDto);
    }

    @GetMapping(value = "get/{regionId}", produces = "application/json")
    @ApiOperation(value = "Возврат область по идентификатору", response = RegionDto.class, responseContainer = "One")
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public RegionDto get(
            @ApiParam(value = "Идентификатор", example = "1") @PathVariable Long regionId
    ) {
        log.trace("Accessing GET api/regions/get/{}", regionId);

        Region region = regions.get(regionId);

        log.trace("{} get region and returned to front-end", region);

        return regionMapper.toDto(region);
    }

    @PostMapping(value = "modify", produces = "application/json")
    @ApiOperation(value = "Создание или редактирование область на основе предоставленной информации", response = RegionDto.class, responseContainer = "One")
    @Transactional
    public RegionDto modify(
            @ApiParam("Запрос объекта") @RequestBody RegionModifyDto dto
    ) {
        log.trace("Accessing POST api/regions/modify/{} ", dto.toJsonString());

        Region region = regions.modify(dto);

        log.trace("Region with regionId={} modify region and returned to front-end", region.getId());

        return regionMapper.toDto(region);
    }

    @DeleteMapping(value = "delete/{regionId}", produces = "application/json")
    @ApiOperation(value = "Удалить область с заданным идентификатором", responseContainer = "void")
    @Transactional
    public void delete(
            @ApiParam(value = "Запрос списка", example = "1") @PathVariable Long regionId
    ) {
        log.trace("Accessing GET api/regions");

        regions.delete(regionId);

        log.trace("{} deleted region and returned void to front-end", regionId);
    }
}
