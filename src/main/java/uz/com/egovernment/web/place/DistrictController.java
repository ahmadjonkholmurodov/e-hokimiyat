package uz.com.egovernment.web.place;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import uz.com.egovernment.api.place.Districts;
import uz.com.egovernment.domain.place.District;
import uz.com.egovernment.dto.place.DistrictDto;
import uz.com.egovernment.dto.place.DistrictModifyDto;
import uz.com.egovernment.mapper.place.DistrictMapper;
import uz.com.egovernment.pagination.place.DistrictPageableSearch;

@Slf4j
@RestController
@RequestMapping("api/districts")
@ApiModel("Точка доступа для район")
public class DistrictController {

    private final Districts districts;
    private final DistrictMapper districtMapper;

    @Autowired
    public DistrictController(Districts districts, DistrictMapper districtMapper) {
        this.districts = districts;
        this.districtMapper = districtMapper;
    }

    @PostMapping(value = "search", produces = "application/json")
    @ApiOperation(value = "Получение списка пользователей с пагинацией и сортировкой", response = DistrictDto.class, responseContainer = "List")
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Page<DistrictDto> list(
            @ApiParam("Запрос списка") @RequestBody DistrictPageableSearch search
    ) {
        log.trace("Accessing GET api/districts/search/{}", search);

        Page<District> foundDistricts = districts.list(search);

        log.trace("{} districts found and {} returned to front-end", foundDistricts.getTotalElements(), foundDistricts.getSize());

        return foundDistricts.map(districtMapper::toDto);
    }

    @GetMapping(value = "get/{districtId}", produces = "application/json")
    @ApiOperation(value = "Возврат район по идентификатору", response = DistrictDto.class, responseContainer = "One")
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public DistrictDto get(
            @ApiParam(value = "Идентификатор", example = "1") @PathVariable Long districtId
    ) {
        log.trace("Accessing GET api/districts/get/{}", districtId);

        District district = districts.get(districtId);

        log.trace("{} get district and returned to front-end", district);

        return districtMapper.toDto(district);
    }

    @PostMapping(value = "modify", produces = "application/json")
    @ApiOperation(value = "Создание или редактирование район на основе предоставленной информации", response = DistrictDto.class, responseContainer = "One")
    @Transactional
    public DistrictDto modify(
            @ApiParam("Запрос объекта") @RequestBody DistrictModifyDto dto
    ) {
        log.trace("Accessing POST api/districts/modify/{} ", dto.toJsonString());

        District district = districts.modify(dto);

        log.trace("District with districtId={} modify district and returned to front-end", district.getId());

        return districtMapper.toDto(district);
    }

    @DeleteMapping(value = "delete/{districtId}", produces = "application/json")
    @ApiOperation(value = "Удалить район с заданным идентификатором", responseContainer = "void")
    @Transactional
    public void delete(
            @ApiParam(value = "Запрос списка", example = "1") @PathVariable Long districtId
    ) {
        log.trace("Accessing GET api/districts");

        districts.delete(districtId);

        log.trace("{} deleted district and returned void to front-end", districtId);
    }
}
