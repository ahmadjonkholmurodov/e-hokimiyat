package uz.com.egovernment.web.place;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import uz.com.egovernment.api.place.Mahallas;
import uz.com.egovernment.domain.place.Mahalla;
import uz.com.egovernment.dto.place.MahallaDto;
import uz.com.egovernment.dto.place.MahallaModifyDto;
import uz.com.egovernment.mapper.place.MahallaMapper;
import uz.com.egovernment.pagination.place.MahallaPageableSearch;

@Slf4j
@RestController
@RequestMapping("api/mahallas")
@ApiModel("Точка доступа для Mahalla")
public class MahallaController {

    private final Mahallas mahallas;
    private final MahallaMapper mahallaMapper;

    @Autowired
    public MahallaController(Mahallas mahallas, MahallaMapper mahallaMapper) {
        this.mahallas = mahallas;
        this.mahallaMapper = mahallaMapper;
    }

    @PostMapping(value = "search", produces = "application/json")
    @ApiOperation(value = "Получение списка пользователей с пагинацией и сортировкой", response = MahallaDto.class, responseContainer = "List")
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Page<MahallaDto> list(
            @ApiParam("Запрос списка") @RequestBody MahallaPageableSearch search
    ) {
        log.trace("Accessing GET api/mahallas/search/{}", search);

        Page<Mahalla> foundMahallas = mahallas.list(search);

        log.trace("{} mahallas found and {} returned to front-end", foundMahallas.getTotalElements(), foundMahallas.getSize());

        return foundMahallas.map(mahallaMapper::toDto);
    }

    @GetMapping(value = "get/{mahallaId}", produces = "application/json")
    @ApiOperation(value = "Возврат Сектор по идентификатору", response = MahallaDto.class, responseContainer = "One")
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public MahallaDto get(
            @ApiParam(value = "Идентификатор", example = "1") @PathVariable Long mahallaId
    ) {
        log.trace("Accessing GET api/mahallas/get/{}", mahallaId);

        Mahalla mahalla = mahallas.get(mahallaId);

        log.trace("{} get mahalla and returned to front-end", mahalla);

        return mahallaMapper.toDto(mahalla);
    }

    @PostMapping(value = "modify", produces = "application/json")
    @ApiOperation(value = "Создание или редактирование Mahalla на основе предоставленной информации", response = MahallaDto.class, responseContainer = "One")
    @Transactional
    public MahallaDto modify(
            @ApiParam("Запрос объекта") @RequestBody MahallaModifyDto dto
    ) {
        log.trace("Accessing POST api/mahallas/modify/{} ", dto.toJsonString());

        Mahalla mahalla = mahallas.modify(dto);

        log.trace("Mahalla with mahallaId={} modify mahalla and returned to front-end", mahalla.getId());

        return mahallaMapper.toDto(mahalla);
    }

    @DeleteMapping(value = "delete/{mahallaId}", produces = "application/json")
    @ApiOperation(value = "Удалить Mahalla с заданным идентификатором", responseContainer = "void")
    @Transactional
    public void delete(
            @ApiParam(value = "Запрос списка", example = "1") @PathVariable Long mahallaId
    ) {
        log.trace("Accessing GET api/mahallas");

        mahallas.delete(mahallaId);

        log.trace("{} deleted mahalla and returned void to front-end", mahallaId);
    }
}
