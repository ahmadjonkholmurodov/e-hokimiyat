package uz.com.egovernment.web.place;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import uz.com.egovernment.api.place.Sectors;
import uz.com.egovernment.domain.place.Sector;
import uz.com.egovernment.dto.place.SectorDto;
import uz.com.egovernment.dto.place.SectorModifyDto;
import uz.com.egovernment.mapper.place.SectorMapper;
import uz.com.egovernment.pagination.place.SectorPageableSearch;

@Slf4j
@RestController
@RequestMapping("api/sectors")
@ApiModel("Точка доступа для Сектор")
public class SectorController {

    private final Sectors sectors;
    private final SectorMapper sectorMapper;

    @Autowired
    public SectorController(Sectors sectors, SectorMapper sectorMapper) {
        this.sectors = sectors;
        this.sectorMapper = sectorMapper;
    }

    @PostMapping(value = "search", produces = "application/json")
    @ApiOperation(value = "Получение списка пользователей с пагинацией и сортировкой", response = SectorDto.class, responseContainer = "List")
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Page<SectorDto> list(
            @ApiParam("Запрос списка") @RequestBody SectorPageableSearch search
    ) {
        log.trace("Accessing GET api/sectors/search/{}", search);

        Page<Sector> foundSectors = sectors.list(search);

        log.trace("{} sectors found and {} returned to front-end", foundSectors.getTotalElements(), foundSectors.getSize());

        return foundSectors.map(sectorMapper::toDto);
    }

    @GetMapping(value = "get/{sectorId}", produces = "application/json")
    @ApiOperation(value = "Возврат Сектор по идентификатору", response = SectorDto.class, responseContainer = "One")
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public SectorDto get(
            @ApiParam(value = "Идентификатор", example = "1") @PathVariable Long sectorId
    ) {
        log.trace("Accessing GET api/sectors/get/{}", sectorId);

        Sector sector = sectors.get(sectorId);

        log.trace("{} get sector and returned to front-end", sector);

        return sectorMapper.toDto(sector);
    }

    @PostMapping(value = "modify", produces = "application/json")
    @ApiOperation(value = "Создание или редактирование Сектор на основе предоставленной информации", response = SectorDto.class, responseContainer = "One")
    @Transactional
    public SectorDto modify(
            @ApiParam("Запрос объекта") @RequestBody SectorModifyDto dto
    ) {
        log.trace("Accessing POST api/sectors/modify/{} ", dto.toJsonString());

        Sector sector = sectors.modify(dto);

        log.trace("Sector with sectorId={} modify sector and returned to front-end", sector.getId());

        return sectorMapper.toDto(sector);
    }

    @DeleteMapping(value = "delete/{sectorId}", produces = "application/json")
    @ApiOperation(value = "Удалить Сектор с заданным идентификатором", responseContainer = "void")
    @Transactional
    public void delete(
            @ApiParam(value = "Запрос списка", example = "1") @PathVariable Long sectorId
    ) {
        log.trace("Accessing GET api/sectors");

        sectors.delete(sectorId);

        log.trace("{} deleted sector and returned void to front-end", sectorId);
    }
}
