package uz.com.egovernment.dao.place;

import org.springframework.stereotype.Repository;
import uz.com.egovernment.dao.SoftDeleteJpaRepository;
import uz.com.egovernment.domain.place.District;

@Repository
public interface DistrictRepository extends SoftDeleteJpaRepository<District> {
}
