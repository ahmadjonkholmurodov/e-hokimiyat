package uz.com.egovernment.dao.place;

import org.springframework.stereotype.Repository;
import uz.com.egovernment.dao.SoftDeleteJpaRepository;
import uz.com.egovernment.domain.place.Street;

@Repository
public interface StreetRepository extends SoftDeleteJpaRepository<Street> {
}
