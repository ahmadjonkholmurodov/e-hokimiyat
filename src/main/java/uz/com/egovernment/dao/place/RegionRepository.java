package uz.com.egovernment.dao.place;

import org.springframework.stereotype.Repository;
import uz.com.egovernment.dao.SoftDeleteJpaRepository;
import uz.com.egovernment.domain.place.Region;

@Repository
public interface RegionRepository extends SoftDeleteJpaRepository<Region> {
}
