package uz.com.egovernment.dao.place;

import org.springframework.stereotype.Repository;
import uz.com.egovernment.dao.SoftDeleteJpaRepository;
import uz.com.egovernment.domain.place.Mahalla;

@Repository
public interface MahallaRepository extends SoftDeleteJpaRepository<Mahalla> {
}
