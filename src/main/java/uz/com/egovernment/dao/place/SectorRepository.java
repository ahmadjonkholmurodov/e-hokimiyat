package uz.com.egovernment.dao.place;

import org.springframework.stereotype.Repository;
import uz.com.egovernment.dao.SoftDeleteJpaRepository;
import uz.com.egovernment.domain.place.Sector;

@Repository
public interface SectorRepository extends SoftDeleteJpaRepository<Sector> {

}
