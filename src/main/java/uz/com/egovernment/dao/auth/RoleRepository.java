package uz.com.egovernment.dao.auth;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.com.egovernment.dao.SoftDeleteJpaRepository;
import uz.com.egovernment.domain.auth.Role;

import java.util.Optional;

@Repository
public interface RoleRepository extends SoftDeleteJpaRepository<Role> {

    @Modifying
    @Query(value ="delete from role_permissions", nativeQuery = true)
    void deleteAllByRelations();

    Optional<Role> findFirstByName(String name);
}
