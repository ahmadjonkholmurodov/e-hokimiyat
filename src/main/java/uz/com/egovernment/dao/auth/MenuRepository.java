package uz.com.egovernment.dao.auth;

import org.springframework.stereotype.Repository;
import uz.com.egovernment.dao.SoftDeleteJpaRepository;
import uz.com.egovernment.domain.auth.Menu;

@Repository
public interface MenuRepository extends SoftDeleteJpaRepository<Menu> {
}
