package uz.com.egovernment.dao.auth;

import org.springframework.stereotype.Repository;
import uz.com.egovernment.dao.SoftDeleteJpaRepository;
import uz.com.egovernment.domain.auth.Action;

@Repository
public interface ActionRepository extends SoftDeleteJpaRepository<Action> {
}
