package uz.com.egovernment.dao.auth;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import uz.com.egovernment.dao.SoftDeleteJpaRepository;
import uz.com.egovernment.domain.auth.LocalUser;

import java.util.Optional;

@Repository
public interface UserRepository extends SoftDeleteJpaRepository<LocalUser> {

    @Modifying
    @Query(value = "delete from user_roles", nativeQuery = true)
    void deleteAllByRoleRelations();

    @Query(value = "select * from users where username = :userName", nativeQuery = true)
    Optional<LocalUser> findByUsernameNative(@Param("userName") String userName);

    @Query(value = "select * from users where email = :email", nativeQuery = true)
    Optional<LocalUser> findByEmailNative(@Param("email") String email);

    @Modifying
    @Query(value = "delete from users", nativeQuery = true)
    void deleteAll();

    Optional<LocalUser> findFirstByUsername(String username);

    Optional<LocalUser> findByUsernameAndIdIsNot(String username, Long id);

    Optional<LocalUser> findByEmailAndIdIsNot(String email, Long id);
}
