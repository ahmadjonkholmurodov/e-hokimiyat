package uz.com.egovernment.dao.auth;

import org.springframework.stereotype.Repository;
import uz.com.egovernment.dao.SoftDeleteJpaRepository;
import uz.com.egovernment.domain.auth.MenuAction;

@Repository
public interface MenuActionRepository extends SoftDeleteJpaRepository<MenuAction> {

}
