package uz.com.egovernment.pagination.auth;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import uz.com.egovernment.pagination.PageableSearch;

@Getter
@Setter
@ToString
@NoArgsConstructor
@ApiModel("Запрос на поиск по списку Пользователь")
public class UserSearch extends PageableSearch {

    private String username;

    private String name;

}
