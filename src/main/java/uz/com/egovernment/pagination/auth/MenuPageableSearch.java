package uz.com.egovernment.pagination.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import uz.com.egovernment.pagination.PageableSearch;

@Getter
@Setter
@ToString
@NoArgsConstructor
@ApiModel("Запрос на поиск по списку меню")
public class MenuPageableSearch extends PageableSearch {

    @ApiModelProperty(value = "Подстрока для поиска по имени роли", example = "Administrator")
    private String name;

    @ApiModelProperty(value = "идентификатор меню родитель", example = "1")
    private Long parentId;
}
