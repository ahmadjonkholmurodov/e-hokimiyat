package uz.com.egovernment.pagination.place;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import uz.com.egovernment.pagination.PageableSearch;

@Getter
@Setter
@ToString
@NoArgsConstructor
@ApiModel("Запрос на поиск по списку улица")
public class StreetPageableSearch extends PageableSearch {

    @ApiModelProperty(value = "название улица", example = "Allama улица")
    private String name;

    @ApiModelProperty(value = "Идентификатор mahalla", example = "1")
    private Long mahallaId;
}
