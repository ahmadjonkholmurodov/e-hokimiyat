package uz.com.egovernment.pagination.place;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import uz.com.egovernment.pagination.PageableSearch;

@Getter
@Setter
@ToString
@NoArgsConstructor
@ApiModel("Запрос на поиск по списку сектор")
public class SectorPageableSearch extends PageableSearch {
    @ApiModelProperty(value = "Подстрока для поиска по имени сектор", example = "A")
    private String name;

    @ApiModelProperty(value = "Идентификатор район", example = "1")
    private Long districtId;
}
