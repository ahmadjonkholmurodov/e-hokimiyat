package uz.com.egovernment.pagination.place;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import uz.com.egovernment.pagination.PageableSearch;

@Getter
@Setter
@ToString
@NoArgsConstructor
@ApiModel("Запрос на поиск по списку район")
public class DistrictPageableSearch extends PageableSearch {

    @ApiModelProperty(value = "название район", example = "Baliqchi")
    private String name;

    @ApiModelProperty(value = "Идентификатор область", example = "1")
    private Long regionId;
}
