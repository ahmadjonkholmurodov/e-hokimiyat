package uz.com.egovernment.pagination.place;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import uz.com.egovernment.pagination.PageableSearch;

@Getter
@Setter
@ToString
@NoArgsConstructor
@ApiModel("Запрос на поиск по списку область")
public class RegionPageableSearch extends PageableSearch {

    @ApiModelProperty(value = "Подстрока для поиска по имени область", example = "Andijon")
    private String name;
}
