package uz.com.egovernment.dto.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("Меню")
public class MenuDto {

    @ApiModelProperty(value = "Идентификатор Меню", example = "1")
    private Long id;

    @ApiModelProperty(value = "название Меню", example = "SIDE_BAR")
    private String name;

    @ApiModelProperty(value = "название Меню (Cy)", example = "SIDE_BAR")
    private String nameCy;

    private Boolean isPage;

    private Boolean enabled;

    @ApiModelProperty(value = "идентификатор меню родитель", example = "1")
    private Long parentId;

    @ApiModelProperty(value = "название меню родитель", example = "SIDE_BAR")
    private String parentName;
}
