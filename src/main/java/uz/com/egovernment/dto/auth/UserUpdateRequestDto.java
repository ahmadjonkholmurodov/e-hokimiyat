package uz.com.egovernment.dto.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uz.com.egovernment.dto.AbstractRequest;
import uz.com.egovernment.dto.enums.Languages;

import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("Обновить системного Пользователя")
public class UserUpdateRequestDto implements AbstractRequest {

    @ApiModelProperty(value = "Идентификатор Пользователя", example = "1", required = true)
    private Long id;

    @ApiModelProperty(value = "имя пользователя")
    private String userName;

    @ApiModelProperty(value = "пароль пользователя")
    private String password;

    @ApiModelProperty(value = "электронная почта пользователя")
    private String email;

    @ApiModelProperty(value = "телефонный номер пользователя")
    private String phoneNumber;

    @ApiModelProperty(value = "язык пользователя системы")
    private Languages language;

    @Size(max = 100, message = " max size %s")
    private String firstName;

    @Size(max = 100, message = " max size %s")
    private String middleName;

    @Size(max = 100, message = " max size %s")
    private String lastName;
}
