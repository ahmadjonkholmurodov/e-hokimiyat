package uz.com.egovernment.dto.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@AllArgsConstructor
@ApiModel("Ответ успешной авторизации")
public class LoginResponse {

    @ApiModelProperty("Токен авторизации, который в дальнейшем нужно передавать в соответствующем header для всех последующих запросов")
    private final String token;

    @ApiModelProperty("информация о пользователе сеанса")
    private final UserInfoDto userInfo;
}