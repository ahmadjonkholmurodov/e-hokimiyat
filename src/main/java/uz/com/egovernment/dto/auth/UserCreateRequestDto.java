package uz.com.egovernment.dto.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uz.com.egovernment.dto.AbstractRequest;

import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("Создать системного пользователя")
public class UserCreateRequestDto implements AbstractRequest {

    @ApiModelProperty(value = "имя пользователя", required = true)
    private String userName;

    @ApiModelProperty(value = "пароль пользователя", required = true)
    private String password;

    @ApiModelProperty(value = "электронная почта пользователя")
    private String email;

    @ApiModelProperty(value = "телефонный номер пользователя")
    private String phoneNumber;

    @Size(max = 100, message = " max size %s")
    private String firstName;

    @Size(max = 100, message = " max size %s")
    private String middleName;

    @Size(max = 100, message = " max size %s")
    private String lastName;
}
