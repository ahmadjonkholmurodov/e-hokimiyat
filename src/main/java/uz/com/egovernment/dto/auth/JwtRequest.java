package uz.com.egovernment.dto.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = "password")
@ApiModel("Запрос авторизации")
public class JwtRequest {

    @ApiModelProperty("Имя пользователя")
    private String username;

    @ApiModelProperty("Пароль пользователя")
    private String password;
}