package uz.com.egovernment.dto.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uz.com.egovernment.dto.AbstractRequest;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("CRUD меню действий")
public class MenuActionCreateDto implements AbstractRequest {

    @ApiModelProperty(value = "идентификатор роль", example = "1")
    private Long roleId;

    @ApiModelProperty(value = "идентификатор меню", example = "1")
    private Long menuId;

    @ApiModelProperty(value = "идентификатор действие", example = "1")
    private Long actionId;
}
