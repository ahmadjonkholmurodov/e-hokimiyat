package uz.com.egovernment.dto.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import uz.com.egovernment.dto.AbstractRequest;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("CRUD роли")
public class RoleModifyDto implements AbstractRequest {

    @ApiModelProperty(value = "Идентификатор роли", example = "1")
    private Long id;

    @ApiModelProperty(value = "название роли", example = "ADMIN")
    private String name;

    @ApiModelProperty(value = "код роли только латиницей", example = "ADMIN")
    private String code;
}
