package uz.com.egovernment.dto.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uz.com.egovernment.dto.enums.Languages;
import uz.com.egovernment.dto.enums.UserStateType;

import javax.validation.constraints.Size;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("Пользователя")
public class UserInfoDto {

    @ApiModelProperty(value = "Идентификатор Пользователя", example = "1", required = true)
    private Long id;

    @ApiModelProperty(value = "язык пользователя системы")
    private Languages language;

    @ApiModelProperty(value = "доступ пользователей к системе")
    protected UserStateType state;

    @Size(max = 100, message = " max size %s")
    private String firstName;

    @Size(max = 100, message = " max size %s")
    private String middleName;

    @Size(max = 100, message = " max size %s")
    private String lastName;

    private Set<String> roles;

}
