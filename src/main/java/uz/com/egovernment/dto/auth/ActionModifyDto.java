package uz.com.egovernment.dto.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uz.com.egovernment.dto.AbstractRequest;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("CRUD действие")
public class ActionModifyDto implements AbstractRequest {

    @ApiModelProperty(value = "Идентификатор роли", example = "1")
    private Long id;

    @ApiModelProperty(value = "название код", example = "ADMIN")
    private String code;

    @ApiModelProperty(value = "описание", example = "Test test")
    private String description;

}
