package uz.com.egovernment.dto.place;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uz.com.egovernment.dto.AbstractRequest;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("CRUD Область")
public class DistrictModifyDto implements AbstractRequest {

    @ApiModelProperty(value = "Идентификатор район", example = "1")
    private Long id;

    @ApiModelProperty(value = "название район", example = "Baliqchi")
    private String name;

    @ApiModelProperty(value = "название (Cy) район", example = "Балыкчи")
    private String nameCy;

    @ApiModelProperty(value = "Идентификатор область", example = "1")
    private Long regionId;
}
