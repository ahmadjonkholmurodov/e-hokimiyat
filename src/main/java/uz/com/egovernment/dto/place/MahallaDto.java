package uz.com.egovernment.dto.place;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("Mahalla")
public class MahallaDto {

    @ApiModelProperty(value = "Идентификатор mahalla", example = "1")
    private Long id;

    @ApiModelProperty(value = "название mahalla", example = "Allama mfy")
    private String name;

    @ApiModelProperty(value = "название (Cy) mahalla", example = "Allama mfy")
    private String nameCy;

    @ApiModelProperty(value = "Идентификатор сектор", example = "1")
    private Long sectorId;

    @ApiModelProperty(value = "название сектор", example = "A")
    private String sectorName;
}
