package uz.com.egovernment.dto.place;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uz.com.egovernment.dto.AbstractRequest;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("CRUD Улица")
public class StreetModifyDto implements AbstractRequest {

    @ApiModelProperty(value = "Идентификатор улица", example = "1")
    private Long id;

    @ApiModelProperty(value = "название улица", example = "Allama улица")
    private String name;

    @ApiModelProperty(value = "название (Cy) улица", example = "Allama улица")
    private String nameCy;

    @ApiModelProperty(value = "Идентификатор mahalla", example = "1")
    private Long mahallaId;
}
