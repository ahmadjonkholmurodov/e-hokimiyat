package uz.com.egovernment.dto.place;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("Область")
public class RegionDto {

    @ApiModelProperty(value = "Идентификатор область", example = "1")
    private Long id;

    @ApiModelProperty(value = "название область", example = "Andijon")
    private String name;

    @ApiModelProperty(value = "название (Cy) область", example = "Андижан")
    private String nameCy;
}
