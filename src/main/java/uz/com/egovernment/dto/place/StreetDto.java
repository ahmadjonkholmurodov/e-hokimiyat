package uz.com.egovernment.dto.place;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("Улица")
public class StreetDto {

    @ApiModelProperty(value = "Идентификатор улица", example = "1")
    private Long id;

    @ApiModelProperty(value = "название улица", example = "Allama улица")
    private String name;

    @ApiModelProperty(value = "название (Cy) улица", example = "Allama улица")
    private String nameCy;

    @ApiModelProperty(value = "Идентификатор mahalla", example = "1")
    private Long mahallaId;

    @ApiModelProperty(value = "название mahalla", example = "Allama mfy")
    private String mahallaName;
}
