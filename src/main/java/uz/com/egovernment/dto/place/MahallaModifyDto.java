package uz.com.egovernment.dto.place;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uz.com.egovernment.dto.AbstractRequest;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("CRUD Mahalla")
public class MahallaModifyDto implements AbstractRequest {

    @ApiModelProperty(value = "Идентификатор mahalla", example = "1")
    private Long id;

    @ApiModelProperty(value = "название mahalla", example = "Allama mfy")
    private String name;

    @ApiModelProperty(value = "название (Cy) mahalla", example = "Allama mfy")
    private String nameCy;

    @ApiModelProperty(value = "Идентификатор сектор", example = "1")
    private Long sectorId;
}
