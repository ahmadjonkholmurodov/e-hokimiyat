package uz.com.egovernment.dto.place;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("Сектор")
public class SectorDto {

    @ApiModelProperty(value = "Идентификатор сектор", example = "1")
    private Long id;

    @ApiModelProperty(value = "название сектор", example = "A")
    private String name;

    @ApiModelProperty(value = "название (Cy) сектор", example = "A")
    private String nameCy;

    @ApiModelProperty(value = "Идентификатор район", example = "1")
    private Long districtId;

    @ApiModelProperty(value = "название район", example = "Baliqchi")
    private String districtName;
}
