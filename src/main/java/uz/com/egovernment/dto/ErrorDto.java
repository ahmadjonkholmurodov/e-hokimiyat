package uz.com.egovernment.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
@Getter
@ApiModel(description = "Ответ в виде вообщения об ошибке")
public class ErrorDto {

    @ApiModelProperty(value = "Код ошибки используемый в том числе для получения шаблона локализации", example = "error.validation_error")
    private final String code;

    @ApiModelProperty(value = "Некоторые параметры ошибки, которые должны вставляться в шаблон сообщения об ошибке. "
            + "Шаблон сообщения следует получить через механизм локализации из code")
    private final List<Serializable> params;
}
