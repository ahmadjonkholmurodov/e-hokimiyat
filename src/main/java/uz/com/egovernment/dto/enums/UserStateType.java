package uz.com.egovernment.dto.enums;

public enum UserStateType {

    ACTIVE("ACTIVE"),
    INACTIVE("INACTIVE"),
    BLOCKED("BLOCKED");

    public final String code;

    UserStateType(String code) {
        this.code = code;
    }
}
