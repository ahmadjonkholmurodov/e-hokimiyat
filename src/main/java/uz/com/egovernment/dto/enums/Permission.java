package uz.com.egovernment.dto.enums;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

public enum Permission {

    ADMIN(null),
    USER(null),
    USER_MODIFY(USER),
    USER_DELETE(USER),
    USER_ATTACH_ROLE(USER),
    USER_VIEW(USER),

    ROLE(null),
    ROLE_MODIFY(ROLE),
    ROLE_DELETE(ROLE),
    ROLE_ATTACH_PERMISSION(ROLE),
    ROLE_VIEW(ROLE),
    ;

    private final Permission parent;

    Permission(Permission parent) {
        this.parent = parent;
    }

    static public Set<Permission> getParents() {
        return Arrays.stream(values()).filter(t -> t.parent == null).collect(Collectors.toSet());
    }

    static public Set<Permission> getChildren(Permission permission) {
        return Arrays.stream(values()).filter(t -> t.parent == permission).collect(Collectors.toSet());
    }
}
