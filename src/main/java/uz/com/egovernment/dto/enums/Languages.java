package uz.com.egovernment.dto.enums;

public enum Languages {
    EN,
    RU,
    UZ
}
