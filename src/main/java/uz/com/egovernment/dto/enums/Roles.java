package uz.com.egovernment.dto.enums;

public enum Roles {

    ROLE_ADMIN("Админ");

    final String description;

    Roles(String description) {
        this.description = description;
    }
}
