package uz.com.egovernment.mapper.place;

import org.mapstruct.Mapper;
import uz.com.egovernment.domain.place.Sector;
import uz.com.egovernment.dto.place.SectorDto;

@Mapper(componentModel = "spring")
public interface SectorMapper {

    SectorDto toDto(Sector sector);
}
