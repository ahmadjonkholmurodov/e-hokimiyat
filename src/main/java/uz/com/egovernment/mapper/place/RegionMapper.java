package uz.com.egovernment.mapper.place;

import org.mapstruct.Mapper;
import uz.com.egovernment.domain.place.Region;
import uz.com.egovernment.dto.place.RegionDto;

@Mapper(componentModel = "spring")
public interface RegionMapper {

    RegionDto toDto(Region region);
}
