package uz.com.egovernment.mapper.place;

import org.mapstruct.Mapper;
import uz.com.egovernment.domain.place.District;
import uz.com.egovernment.dto.place.DistrictDto;

@Mapper(componentModel = "spring")
public interface DistrictMapper {

    DistrictDto toDto(District district);
}
