package uz.com.egovernment.mapper.place;

import org.mapstruct.Mapper;
import uz.com.egovernment.domain.place.Street;
import uz.com.egovernment.dto.place.StreetDto;

@Mapper(componentModel = "spring")
public interface StreetMapper {

    StreetDto toDto(Street street);
}
