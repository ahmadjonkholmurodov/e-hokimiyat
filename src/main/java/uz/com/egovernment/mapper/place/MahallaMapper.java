package uz.com.egovernment.mapper.place;

import org.mapstruct.Mapper;
import uz.com.egovernment.domain.place.Mahalla;
import uz.com.egovernment.dto.place.MahallaDto;

@Mapper(componentModel = "spring")
public interface MahallaMapper {

    MahallaDto toDto(Mahalla mahalla);
}
