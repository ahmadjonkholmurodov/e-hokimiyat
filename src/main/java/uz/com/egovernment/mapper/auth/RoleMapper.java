package uz.com.egovernment.mapper.auth;

import org.mapstruct.Mapper;
import uz.com.egovernment.domain.auth.Role;
import uz.com.egovernment.dto.auth.RoleDto;

@Mapper(componentModel = "spring")
public interface RoleMapper {

    RoleDto toDto(Role role);
}
