package uz.com.egovernment.mapper.auth;

import org.mapstruct.Mapper;
import uz.com.egovernment.domain.auth.Action;
import uz.com.egovernment.dto.auth.ActionDto;

@Mapper(componentModel = "spring")
public interface ActionMapper {

    ActionDto toDto(Action action);
}
