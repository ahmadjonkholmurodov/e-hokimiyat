package uz.com.egovernment.mapper.auth;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import uz.com.egovernment.domain.auth.Menu;
import uz.com.egovernment.dto.auth.MenuDto;

@Mapper(componentModel = "spring")
public interface MenuMapper {

    @Mappings({
            @Mapping(target = "parentId", source = "parent.id"),
            @Mapping(target = "parentName", source = "parent.name")
    })
    MenuDto toDto(Menu menu);
}
