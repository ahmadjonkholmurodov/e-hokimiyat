package uz.com.egovernment.mapper.auth;

import org.mapstruct.Mapper;
import uz.com.egovernment.domain.auth.LocalUser;
import uz.com.egovernment.dto.auth.UserDto;

@Mapper(componentModel = "spring")
public interface UserMapper {

    UserDto toDto(LocalUser localUser);
}
